using System.Collections.Generic;

public class BattleResults
{
    public ResourceList ResourcesLost;
    public ResourceList ResourcesGained;
    public BattleTeam Winner;
    public BattleTeam Loser;

    public string GetResultsString()
    {
        
        var masterString = "";
        if (Winner.isHuman)
        {
            masterString += "YOU WON THE BATTLE!";
            if (ResourcesGained.Count > 0)
            {
                var gainString = "\nGAINED:";
                foreach (KeyValuePair<ResourceType, Resource> gainedResource in ResourcesGained)
                {
                    gainString += gainedResource.Value.GetResourceTextString();
                }

                masterString += gainString;
            }
        }
        else
        {
            masterString += "YOU LOST THE BATTLE...";
        }
        if (ResourcesLost.Count > 0)
        {
            var lossString = "\nLOST:";

            foreach (KeyValuePair<ResourceType,Resource> lostResource in ResourcesLost)
            {
                lossString += lostResource.Value.GetResourceTextString();
            }

            masterString += lossString;
        }

       

        return masterString;
    }
}