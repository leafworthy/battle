﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = System.Random;


public class ButtonControl : MonoBehaviour
{
    private Button _button;
    private float _startTime = 0f;
    private float _bounceTime = 0f;

    private bool _mousePressing = false;
    private bool _outward = true;
    private float _currentScore = 0f;
    private float _timeOfLastPress = 0f;

    public System.Action onPress;
    public System.Action<bool> onScore01;
    public System.Action<float> onScore0100;

    [SerializeField] public ResourceButtonData data;
    [SerializeField] private float maxTime = .5f;
    [SerializeField] private AnimationCurve valueCurve;
    [SerializeField] private Bar pressBar;
    [SerializeField] private Bar cooldownBar;
    [SerializeField] private Bar scoreBar;
    [SerializeField] private Bar targetBar;
    [Range(0, 100)] [SerializeField] private float targetRangeSize = 0;
    [Range(0, 360)] [SerializeField] private float degreesOfRotation = 0;
    [SerializeField] private TextMeshProUGUI textLabel;

    //DEFINED BY ABILITY
    public bool disabled = false;
    [SerializeField] private float coolDownTime = 0f;
    private bool _cooledDown = true;
    private float _timeSinceBounce;
    private bool _buttonPressing = false;


    // Use this for initialization
    void Start()
    {
        _button = GetComponent<Button>();
        pressBar.UpdateBar(0f, 1);
        cooldownBar.UpdateBar(1f, 1);
    }

    private void OnValidate()
    {
        if (textLabel != null)
            textLabel.text = data.ButtonText;
    }

    void Update()
    {
        DetectKeyPresses();
        if (_mousePressing || _buttonPressing)
        {
            if (!_cooledDown)
            {
                if (_outward)
                {
                    GoOutward();
                }
                else
                {
                    GoInward();
                }
            }
        }
        else if (!_cooledDown)
        {
            DoCooldown();
        }
    }

    private void DetectKeyPresses()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            OnButtonDown();
        }
        else
        {
            OnButtonUp();
        }
    }

    private void DoCooldown()
    {
        float duration = Time.time - _timeOfLastPress;
        if (duration < coolDownTime)
        {
            cooldownBar.UpdateBar(duration, coolDownTime);
        }
        else
        {
            Debug.Log("Cooled Down", this);
            _cooledDown = true;
            scoreBar.UpdateBar(0, 1);
            cooldownBar.UpdateBar(1, 1);
            ShowButton();
        }
    }

    private void GoInward()
    {
        _timeSinceBounce = Time.time - _bounceTime;
        pressBar.UpdateBar(maxTime - _timeSinceBounce, maxTime);
        _currentScore = (maxTime - _timeSinceBounce) / maxTime;
    }

    private void GoOutward()
    {
        float duration = Time.time - _startTime;
        if (Time.time - _startTime >= maxTime)
        {
            Bounce();
            pressBar.UpdateBar(maxTime, maxTime);
        }
        else
        {
            pressBar.UpdateBar(duration, maxTime);
            _currentScore = duration / maxTime;
        }
    }

    public void SetDisabled(bool isDisabled = true)
    {
        disabled = isDisabled;
        _button.interactable = !isDisabled;
        //textLabel.enabled = isDisabled;
    }


    public void ShowButton()
    {
        SetDisabled(false);
    }

    public void HideButton()
    {
        SetDisabled(true);
    }

    public float GetScore0_1()
    {
        return Mathf.Clamp(valueCurve.Evaluate(_currentScore), 0f, 1);
    }

    public float GetScore0_100()
    {
        return Mathf.Clamp(valueCurve.Evaluate(_currentScore) * 100, 0f, 100);
    }


    private void Bounce()
    {
        Debug.Log("Bounce", this);
        _outward = false;
        _bounceTime = Time.time;
    }

    private void OnMouseDown()
    {
        if (!_cooledDown)
            return;
        if (!disabled && !_buttonPressing)
        {
            Debug.Log("Press", this);
            _mousePressing = true;
            StartPress();
            onPress?.Invoke();
        }
    }

    private void OnButtonDown()
    {
        if (!_cooledDown)
            return;
        if (!disabled && !_mousePressing && !_buttonPressing)
        {
            _buttonPressing = true;
            Debug.Log("Press", this);
            StartPress();
            onPress?.Invoke();
        }
    }

    private void StartPress()
    {
        if (!_cooledDown)
            return;
        _outward = true;
        _cooledDown = false;
        _currentScore = 0f;
        _startTime = Time.time;
        targetBar.UpdateBar(targetRangeSize, 100);
        degreesOfRotation = UnityEngine.Random.Range(0, 360- degreesOfRotation);
        targetBar.transform.localRotation = Quaternion.AngleAxis(degreesOfRotation,Vector3.forward);
        pressBar.UpdateBar(0f, 1);
    }


    private void OnMouseUp()
    {
        if (!disabled && _mousePressing && !_buttonPressing)
        {
            Score();
        }
    }

    private void OnButtonUp()
    {
        if (!disabled && _buttonPressing && !_mousePressing)
        {
            Score();
        }
    }

 


    private void Score()
    {
        var score = GetScore0_100();
        _mousePressing = false;
        _buttonPressing = false;
        _timeOfLastPress = Time.time;
        pressBar.UpdateBar(0f, 1);
       
        
        onScore0100?.Invoke(score);
        var maxScore = 100 - (degreesOfRotation) / 360 * 100;
        var minScore = maxScore - targetRangeSize;
        Debug.Log("max" +maxScore);
        Debug.Log("min" + minScore);
        Debug.Log("Score: " + score, this);
        if (score <= (maxScore) && score >= (minScore))
        {
           Debug.Log("score is in range");
           onScore01?.Invoke(true);
           scoreBar.UpdateBar(targetRangeSize, 100);
           scoreBar.transform.localRotation = Quaternion.AngleAxis(degreesOfRotation, Vector3.forward);
        }
        else
        {
            Debug.Log("miss");
            onScore01?.Invoke(false);
            scoreBar.UpdateBar(0, 100);
            scoreBar.transform.localRotation = Quaternion.identity;
        }

       
        targetBar.UpdateBar(0, 100);


        
    }
}