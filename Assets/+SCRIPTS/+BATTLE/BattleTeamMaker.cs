using UnityEngine;

public static class BattleTeamMaker 
{
 

    public static BattleTeam CreatePlayerDefenderTeam()
    {
        return new BattleTeam(false, true, PlayerResourceManager.GetPlayerLoot());
    }
    
    public static BattleTeam CreatePlayerAttackerTeam()
    {
        return new BattleTeam(true, true, PlayerResourceManager.GetPlayerLoot());
    }

    public static BattleTeam CreateNewMaraudAttacker()
    {
        return new BattleTeam(true, false, PlayerResourceManager.GetEnemyLoot(false));
       
    }

    public static BattleTeam CreateNewMaraudDefender()
    {
        return new BattleTeam(false, false, PlayerResourceManager.GetEnemyLoot(true));

    }
}