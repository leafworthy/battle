﻿// ReSharper disable once RedundantUsingDirective

using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public enum MenuType
{
    main,
    battle,
    charm,
    showdown
}

public class UIManager : Singleton<UIManager>
{
    public MainMenu mainMenu;
    public CharmMenu charmMenu;
    public ShowdownMenu showdownMenu;
    [FormerlySerializedAs("BattleMenu")] public BattleMenu battleMenu;

    private static Menu CurrentMenu;

    private static string _textString = "";
    public void FixedUpdate()
    {

        if (CurrentMenu != null)
        {
            CurrentMenu.Refresh();
           
        }
    }

    public static string getInfoText()
    {
        return _textString;
    }

    public static void ClearText()
    {
       
        _textString = "";
        Debug.Log("<color=red>cleared</color>");
    }

    public static void DisplayText(string message)
    {
        
        _textString += message;
        Debug.Log(message);
    }

    private static void ShowMenu(Menu menuToShow)
    {
        if (CurrentMenu != null)
        {
            CurrentMenu.Off();
        }

        CurrentMenu = menuToShow;
        CurrentMenu.On();
    }


    public static void OpenMenu(MenuType type)
    {
        switch (type)
        {
            case MenuType.main:
                ShowMenu(I.mainMenu);
                break;
            case MenuType.battle:
                ShowMenu(I.battleMenu);
                break;
            case MenuType.charm:
                ShowMenu(I.charmMenu);
                break;
            case MenuType.showdown:
                ShowMenu(I.showdownMenu);
                break;
            default:
                break;
        }
    }


    public static void DisplayResources()
    {
        CurrentMenu.ResourcesText.text = PlayerResourceManager.GetPlayerResourcesText();
    }

    public static void Init()
    {
        foreach (Menu menu in I.GetComponentsInChildren<Menu>())
        {
            menu.gameObject.SetActive(false);
        }
    }

    public static void DisplayMainMenuText()
    {
       ClearText();
       DisplayText(DayController.GetNightText());
       DisplayText(DayController.GetTodayText());
        DisplayText(BattleManager.GetLatestResults());
        DisplayText("\nSpoo Produced: " + ProductionManager.GetSpooProducedToday());
    }
}