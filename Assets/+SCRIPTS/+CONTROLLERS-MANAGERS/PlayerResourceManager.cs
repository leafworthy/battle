using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Debug = UnityEngine.Debug;

[System.Serializable]
public static class PlayerResourceManager
{
    private static ResourceList _playerResources = new ResourceList();
    private static ResourceDefaultsSO _defaultsSo;
    private static ProductionManager _productionManager = new ProductionManager();

    public static void Init(ResourceDefaultsSO defaultsSo)
    {
        _defaultsSo = defaultsSo;
        AddResourcesScriptableObject(_defaultsSo.DefaultPlayerResources);
        DayController.OnNightFinished += RandomizePrices;
        DayController.OnNightFinished += Produce;
        DayController.OnSleep += Sleep;
    }

    private static void Sleep()
    {
      
    }

    private static void AddResourcesScriptableObject(ResourcesSO newResources)
    {
        PlayerGains(newResources.GetResourceList());
    }

    public static void Produce()
    {
        _productionManager.Produce();
    }

    public static void Trade(ResourceButton button)
    {
        Trade(button.Data.resourceToGain, button.Data.resourceToLose);
    }

    public static void Trade(Resource resourceToGain, Resource resourceToLose)
    {
        PlayerGains(resourceToGain);
        PlayerLoses(resourceToLose);
    }

    public static void RandomizePrices()
    {
        //nothing
    }

    public static void PlayerLoses(Resource lostResource)
    {
        _playerResources.Lose(lostResource);
    }

    public static void PlayerLoses(ResourceList lostResources)
    {
        _playerResources.Lose(lostResources);
    }

    public static void PlayerGains(Resource gainedResource)
    {
        _playerResources.Gain(gainedResource);
    }


    public static void PlayerGains(ResourceList gainedResources)
    {
        _playerResources.Gain(gainedResources);
    }

    public static int GetAmount(ResourceType type)
    {
        return _playerResources.GetAmount(type);
    }

    public static void SetAmount(ResourceType type, int amount)
    {
        _playerResources.SetAmount(type, amount);
    }

    public static string GetPlayerResourcesText()
    {
        string resourcesText = _playerResources.GetResourcesText();
        return resourcesText;
    }


    public static ResourceList GetRandomPlayerLoot()
    {
        var list = new ResourceList();
        list.Gain(_playerResources.GetRandomized());
        return list;
    }

    public static ResourceList GetPlayerLoot()
    {
        var list = new ResourceList();
        foreach (KeyValuePair<ResourceType, Resource> resource in _playerResources)
        {
            list.Gain(resource.Value);
        }

        return list;
    }

    public static ResourceList GetEnemyLoot(bool playerIsAttacker)
    {
        var list = new ResourceList();
        if (playerIsAttacker)
        {
            list.Gain(_defaultsSo.MaxDefenderResourcesScriptableObject.Resources.GetRandomized());
        }
        else
        {
            list.Gain(_defaultsSo.MaxAttackerResourcesScriptableObject.Resources.GetRandomized());
        }

        return list;
    }


    public static void Charm(bool hit)
    {
        if (hit)
        {
            PlayerGains(new Resource(ResourceType.follower, 1, 100));
        }

        PlayerLoses(new Resource(ResourceType.spee, 1, 1));
    }

    private static int GetFollowersCharmed(float score)
    {
        Debug.Log("score");
        var followersCharmed = UnityEngine.Random.Range(0,
            (int) Mathf.Ceil(PlayerResourceManager.GetAmount(ResourceType.spee) * (score / 100)));
        Debug.Log(followersCharmed + " followers charmed");
        return followersCharmed;
    }

    public static int GetTotalItems()
    {
        int total = 0;
        total = _playerResources.Count(t => (t.Key == ResourceType.gem) || (t.Key == ResourceType.gold));
        return total;
    }

    public static void SetMax(ResourceType type, int amount)
    {
        _playerResources.SetMax(type, amount);
    }

    public static void SellAll(ResourceButton button)
    {
        SellAll(button.Data.resourceToGain, button.Data.resourceToLose);
    }

    public static void SellAll(Resource resourceToGain, Resource resourceToLose)
    {
        var amountToSell = GetAmount(resourceToLose.type);
        for (int i = 0; i < amountToSell; i++)
        {
            PlayerLoses(resourceToLose);
            PlayerGains(resourceToGain);
        }
        
    }
}