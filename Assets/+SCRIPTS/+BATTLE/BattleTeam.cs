using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

[System.Serializable]
public class BattleTeam
{
    public BattleTeam(bool isAttacker, bool isHuman, ResourceList loot)
    {
        this.isAttacker = isAttacker;
        this.isHuman = isHuman;
        Loot = loot;
        if (isHuman)
        {
            color = GameController.I.PlayerColor;
            teamName = "Human";
        }
        else
        {
            color = Color.red;
            teamName = "Enemy";
        }
    }

    public string teamName;
    public Action<BattleTeam> OnTeamDie;
    public bool isAttacker;
    public bool isHuman;
    public Color color = Color.white;
    private CombatUnit _leader;
    private BattleTeam _opponent;

    public void SetLeader(CombatUnit leader)
    {
        _leader = leader;
    }

    public void SetOpponent(BattleTeam opponent)
    {
        _opponent = opponent;
    }
    [FormerlySerializedAs("loot")] public ResourceList Loot = new ResourceList();
    [FormerlySerializedAs("startingUnits")] public List<Resource> startingLoot = new List<Resource>();

    private List<CombatUnit> _currentUnits = new List<CombatUnit>();

    public ResourceList GetDeadUnits()
    {
        var currentDeadUnits = new ResourceList();
        foreach (var unit in _currentUnits)
        {
            if (unit.dead)
            {
                Debug.Log("Dead Unit Added: " + unit.unitType.ToString());
                currentDeadUnits.Gain(new Resource(unit.unitType, 1, 100));
            }
        }

        return currentDeadUnits;
    }

    public List<CombatUnit> GetUnits()
    {
        return _currentUnits;
    }

    public void SpawnUnits()
    {
        Debug.Log("Spawning units for: " + this.ToString());
        _currentUnits = new List<CombatUnit>();
        _currentUnits = Spawner.I.SpawnPlayerUnits(this);
        foreach (var currentUnit in _currentUnits)
        {
            Debug.Log(currentUnit.unitType.ToString());
            currentUnit.OnDie += OnUnitDie;
        }
    }

    private void OnUnitDie(CombatUnit unit)
    {
        Debug.Log((unit.GetOwner().isHuman?"Player":"Enemy") + "'s Living Units: " + getLivingUnitsAmount());
        unit.OnDie -= OnUnitDie;
        if (getLivingUnitsAmount() <= 0f)
        {
            Debug.Log("on team die");
            OnTeamDie?.Invoke(this);
        }
    }


    public int getLivingUnitsAmount()
    {
        return _currentUnits.Count(t => !t.dead);
    }

   

    public void Retreat()
    {
        foreach (CombatUnit unit in _currentUnits)
        {
            unit.Retreat();
        }

        _leader.Retreat();
    }

    public void Cheer()
    {
        foreach (CombatUnit unit in _currentUnits)
        {
            unit.Cheer();
        }

        _leader.Cheer();
    }

    public BattleTeam GetOpponent()
    {
        return _opponent;
    }

    public void StartToCastSpecial()
    {
        _leader.DoSpecial();
    }
}