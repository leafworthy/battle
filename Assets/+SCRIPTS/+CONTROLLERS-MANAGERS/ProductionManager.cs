internal class ProductionManager
{
    public int shrineBuildTime = 3;
    public bool shrineBuildingToday = false;
    public static int spooPerFollower = 1;

    private void BuildShrine()
    {
        if (!shrineBuildingToday) return;

        PlayerResourceManager.PlayerGains(new Resource(ResourceType.shrineProgress, 1,1));

        if (PlayerResourceManager.GetAmount(ResourceType.shrineProgress) < shrineBuildTime) return;
        PlayerResourceManager.SetAmount(ResourceType.shrineProgress, 0);
        PlayerResourceManager.PlayerGains(new Resource(ResourceType.shrine, 1,1));
        shrineBuildingToday = false;
    }

    public void StartShrine()
    {
        shrineBuildingToday = true;
        PlayerResourceManager.SetAmount(ResourceType.shrineProgress, 0);
    }

    private void FollowersProduceSpoo()
    {
        PlayerResourceManager.PlayerGains(new Resource(ResourceType.spoo, GetSpooProducedToday(),
            GetSpooProducedToday()));
    }

    public static int GetSpooProducedToday()
    {
        return PlayerResourceManager.GetAmount(ResourceType.follower) * spooPerFollower *
               PlayerResourceManager.GetAmount(ResourceType.shrine);
    }

    private void ResetMaraudAmount()
    {
        PlayerResourceManager.SetAmount(ResourceType.maraud, 1);
    }
    public void Produce()
    {
        BuildShrine();
        FollowersProduceSpoo();
        ResetMaraudAmount();
        ResetSpeeAmount();
        
    }

    private void ResetSpeeAmount()
    {
       PlayerResourceManager.SetAmount(ResourceType.spee, PlayerResourceManager.GetAmount(ResourceType.gold));
       PlayerResourceManager.SetMax(ResourceType.spee, PlayerResourceManager.GetAmount(ResourceType.gold));
    }
}