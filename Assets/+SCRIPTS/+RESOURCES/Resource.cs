using System;
using UnityEngine;


[System.Serializable]
public class Resource
{
    public Resource(ResourceType type, int amount, int max)
    {
        this.type = type;
        this.amount = amount;
        this.max = max;
    }

    public ResourceType type;
    public int amount = 0;
    public int max;

    public string GetCurrencyString()
    {
        switch (type)
        {
            case ResourceType.silver:
                return "$"+amount;
            case ResourceType.gold:
                return amount + "G";
            case ResourceType.spee:
                return amount + " spee";
            case ResourceType.spoo:
                return amount + " spoo";
            default:
                return amount.ToString();
        }
    }

    public void AddTo(Resource resource)
    {
        if ((max < resource.max) && (max != 0))
        {
            
            max = resource.max;
        }

        amount += resource.amount;
        if (max != 0)
        {
            amount = Mathf.Clamp(amount, 0, max);
        }
    }

    public Resource RandomizeAmount()
    {
        var randomAmount = UnityEngine.Random.Range(0, amount);
        return new Resource(type,randomAmount, max);
    }

    public Resource RandomizeUpToAmount()
    {
        var randomAmount = UnityEngine.Random.Range(0, amount);
        return new Resource(type, randomAmount, max);
    }
    public string GetResourceTextString()
    {
        string newString = "";
        switch (type)
        {
            case ResourceType.none:
                break;
            case ResourceType.spee:
                newString = "\nSpee: " + amount + " / " + PlayerResourceManager.GetAmount(ResourceType.gold);
                break;
            case ResourceType.spoo:
                newString = "\nSpoo: " + amount + " / " + max;
                break;
            case ResourceType.silver:
                newString = "\nSilver: $" + amount;
                break;
            case ResourceType.gold:
                newString = "\nGold: " + amount + "G";
                break;
            case ResourceType.gem:
                newString = "\nGems: " + amount;
                break;
            case ResourceType.shrine:
                newString = "\nShrines: " + amount;
                break;
            case ResourceType.maraud:
                newString = "\nRaids Left: " + amount;
                break;
            case ResourceType.follower:
                newString = "\nFollowers: " + amount;
                break;
            case ResourceType.swordsman:
                newString = "\nSwordsmen: " + amount;
                break;
            case ResourceType.biggie:
                newString = "\nBiggies: " + amount;
                break;
            case ResourceType.shrineProgress:
                newString = "\nShrine Progress: " + amount + " / " + max;
                break;
            case ResourceType.wraith:
                newString = "\nWraiths: " + amount;
                break;
            default:
                newString = "";
                break;
        }
        return newString;
    }

    public bool IsValidResource()
    {
        if (type == ResourceType.gem
            | type == ResourceType.gold
            | type == ResourceType.silver
            | type == ResourceType.shrine)
            return true;
        return false;
    }
}