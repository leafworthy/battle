﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum teams
{
    undefined,
    attacker,
    defender
}

public class Spawner : Singleton<Spawner>
{
    public GameObject biggiePrefab;
    public GameObject followerPrefab;
    public GameObject swordsmanPrefab;

    public GameObject wraithPrefab;

    // Start is called before the first frame update
    public static List<CombatUnit> units = new List<CombatUnit>();


    public List<CombatUnit> SpawnPlayerUnits(BattleTeam battleTeam)
    {
        Debug.Log("spawning player units");
        var spawnedUnits = new List<CombatUnit>();
        var newUnit = SpawnNewUnit(wraithPrefab, battleTeam);
        battleTeam.SetLeader(newUnit);
        newUnit.GetComponentInChildren<HideRevealObjects>(true).SetReveal(PlayerResourceManager.GetAmount(ResourceType.gold));
       


        for (int i = 0; i < battleTeam.Loot.GetAmount(ResourceType.swordsman); i++)
        {
            spawnedUnits.Add(SpawnNewUnit(swordsmanPrefab, battleTeam));
        }

        for (int i = 0; i < battleTeam.Loot.GetAmount(ResourceType.biggie); i++)
        {
            spawnedUnits.Add(SpawnNewUnit(biggiePrefab, battleTeam));
        }

        if (!battleTeam.isAttacker)
        {
            for (int i = 0; i < battleTeam.Loot.GetAmount(ResourceType.follower); i++)
            {
                spawnedUnits.Add(SpawnNewUnit(followerPrefab, battleTeam));
            }
        }


        return spawnedUnits;
    }

    private void OnUnitDie(CombatUnit unit)
    {
        //units.Remove(unit);
        unit.OnDie = null;
    }


    private CombatUnit SpawnNewUnit(GameObject currentPrefab, BattleTeam owner)
    {
        Vector2 spawnPosition = owner.isAttacker ? GetRandomScreenLeftPosition() : GetRandomScreenRightPosition();

        var newUnit = Instantiate(currentPrefab, spawnPosition, Quaternion.identity);
        var combat = newUnit.GetComponent<CombatUnit>();
        combat.OnDie += OnUnitDie;
        combat.SetOwner(owner);
        var color = newUnit.GetComponent<ColorSprites>();

        color.Recolor(owner.color);


        units.Add(combat);


        return combat;
    }

    private static Vector2 GetRandomScreenPosition()
    {
        var spawnY = Random.Range
        (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y,
            Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y);
        var spawnX = Random.Range
        (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x,
            Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)).x);
        var spawnPosition = new Vector2(spawnX, spawnY);
        return spawnPosition;
    }

    private static Vector2 GetRandomScreenLeftPosition()
    {
        var spawnY = Random.Range
        (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y,
            Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y);
        var spawnX = Random.Range
        (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x,
            Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, 0)).x);
        var spawnPosition = new Vector2(spawnX, spawnY);
        return spawnPosition;
    }

    private static Vector2 GetRandomScreenRightPosition()
    {
        var spawnY = Random.Range
        (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y,
            Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y);
        var spawnX = Random.Range
        (Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, 0)).x,
            Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)).x);
        var spawnPosition = new Vector2(spawnX, spawnY);
        return spawnPosition;
    }

    // Update is called once per frame


    public void DestroyEverything()
    {
        if (units.Count > 0)
        {
            foreach (var unit in units)
            {
                if (unit != null)
                {
                    Destroy(unit.gameObject);
                }
            }

            units.Clear();
        }
    }
}