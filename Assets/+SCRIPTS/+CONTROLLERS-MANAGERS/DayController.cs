using System;

public static class DayController
{
    private static bool _nightAttack;
    public static int DayNumber = 1;
    public static readonly int MaxDays = 30;
    public static Action OnNightAttack;
    public static Action OnSleep;
    public static Action OnNightFinished;

    public static void Init()
    {
        DayNumber = 1;
    }

    public static void Sleep()
    {
        OnSleep?.Invoke();
        DoNightAttacks();
    }


    private static void NightFinished()
    {
        DayNumber++;
        OnNightFinished?.Invoke();
        UIManager.DisplayMainMenuText();
    }

 

    private static void DoNightAttacks()
    {
        

        if (GetsMarauded() && (DayNumber <= 1))
        {
            _nightAttack = true;
            OnNightAttack?.Invoke();
        }
        else
        {
            _nightAttack = false;
            NightFinished();
        }
    }

    private static bool GetsMarauded()
    {
        var fightsTonightRandom = UnityEngine.Random.Range(0, 10);
        return fightsTonightRandom == 0;
    }


    public static void BattleFinished()
    {
        if (_nightAttack)
        {
            NightFinished();
        }
    }

    public static string GetTodayText()
    {
        string returnString = "Day: " + DayNumber + " / " + MaxDays;
        switch (DayNumber)
        {
            case 1:
                returnString += "\nYou died today. Welcome to your life as a wraith.";
                break;
            case 2:
                returnString += "\nYou survived your first night.";
                break;
            default:
                break;
        }

        return returnString;
    }

    public static string GetNightText()
    {
        return _nightAttack ? "\nYou were attacked in the night." : "\nIt was a normal night.";
    }
}