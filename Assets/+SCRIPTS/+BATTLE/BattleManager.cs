using UnityEngine;
using System.Linq;

public static class BattleManager
{
    private static BattleTeam _attacker;
    private static BattleTeam _defender;
    private static BattleResults _latestResults;
    private static bool _currentlyBattling;
    private static bool _currentlyRetreating;


    public static void Init()
    {
        DayController.OnNightAttack += OnNightAttack;
    }

    private static void OnNightAttack()
    {
        SetupShowdown(BattleTeamMaker.CreateNewMaraudAttacker(), BattleTeamMaker.CreatePlayerDefenderTeam());
    }

    public static void SetupShowdown(BattleTeam attacker, BattleTeam defender)
    {
        _attacker = attacker;
        _defender = defender;
        _defender.SetOpponent(_attacker);
        _attacker.SetOpponent(defender);
        ShowdownMenu.SetTeams(_attacker, _defender);
        UIManager.OpenMenu(MenuType.showdown);
    }

    public static void Fight()
    {
        BattleMenu.SetTeams(_attacker, _defender);
        UIManager.OpenMenu(MenuType.battle);

        _attacker.SpawnUnits();
        _defender.SpawnUnits();
        _attacker.OnTeamDie += OnTeamDie;
        _defender.OnTeamDie += OnTeamDie;
        _currentlyBattling = true;
        _currentlyRetreating = false;
    }

    private static void OnTeamDie(BattleTeam losingTeam)
    {
        BattleFinished(losingTeam);
    }

    private static void OnTeamRetreat(BattleTeam retreatingTeam)
    {
        BattleFinished(retreatingTeam);
    }

    private static BattleResults GetResults(BattleTeam losingTeam)
    {
        var results = losingTeam.isAttacker
            ? GetBattleResults(_defender, _attacker)
            : GetBattleResults(_attacker, _defender);

        _latestResults = results;
        return results;
    }

    private static void BattleFinished(BattleTeam losingTeam)
    {
        _attacker.OnTeamDie -= OnTeamDie;
        _defender.OnTeamDie -= OnTeamDie;
        losingTeam.GetOpponent().Cheer();
        losingTeam.Retreat();
        _currentlyBattling = false;
        BackToMenuSequence(losingTeam);
    }

    public static void BackToMenuSequence(BattleTeam losingTeam)
    {
        Wait.For(3, () => { Finish(losingTeam); });
    }

    private static BattleResults GetBattleResults(BattleTeam winner, BattleTeam loser)
    {
        var results = new BattleResults {ResourcesGained = new ResourceList()};
        results.ResourcesGained.GainValidResources(loser.Loot);
        results.ResourcesLost = new ResourceList();
        results.ResourcesLost.Gain(winner.GetDeadUnits());

        results.Winner = winner;
        results.Loser = loser;
        return results;
    }


    public static void CleanUp()
    {
        Spawner.I.DestroyEverything();
    }


    public static void PlayerPressesAttackButton()
    {
        PlayerResourceManager.PlayerLoses(new Resource(ResourceType.maraud, 1, 1));
        SetupShowdown(BattleTeamMaker.CreatePlayerAttackerTeam(), BattleTeamMaker.CreateNewMaraudDefender());
    }

    public static CombatUnit FindTarget(CombatUnit finder)
    {
        if (finder.unitType == ResourceType.wraith)
        {
            return FindClosestFriendlyTarget(finder);
        }
        else
        {
            return FindClosestEnemyTarget(finder);
        }
    }

    public static void HumanCastSpell()
    {
        if (_attacker.isHuman)
        {
            _attacker.StartToCastSpecial();
        }
        else
        {
            _defender.StartToCastSpecial();
        }
    }

    private static CombatUnit FindClosestEnemyTarget(CombatUnit finder)
    {
        var closestEnemyUnit = Spawner.units.Where(t => finder.TargetIsValid(t))
            .OrderBy(t => Vector2.Distance(finder.transform.position, t.transform.position))
            .FirstOrDefault();
        return closestEnemyUnit;
    }

    private static CombatUnit FindClosestFriendlyTarget(CombatUnit finder)
    {
        var friendlyCombatUnits = Spawner.units.Where(t => ((t.GetOwner() == finder.GetOwner()) &&
                                                            (t.unitType != finder.unitType)));
        var closestFriendlyUnit = friendlyCombatUnits.OrderBy(t =>
            Vector2.Distance(finder.transform.position, t.transform.position)).FirstOrDefault();
        return closestFriendlyUnit;
    }

    public static string GetLatestResults()
    {
        if (_latestResults != null)
        {
            return _latestResults.GetResultsString();
        }

        return "";
    }

    public static void Retreat()
    {
        if (_currentlyBattling && !_currentlyRetreating)
        {
            _currentlyBattling = false;
            _currentlyRetreating = true;
            OnTeamRetreat(_attacker.isHuman ? _attacker : _defender);
        }
    }


    public static void Finish(BattleTeam losingTeam)
    {
        PlayerGainsAndLoses(GetResults(losingTeam));
        DayController.BattleFinished();
        UIManager.OpenMenu(MenuType.main);
    }


    private static void PlayerGainsAndLoses(BattleResults results)
    {
        if (results.Winner.isHuman)
        {
            PlayerResourceManager.PlayerGains(results.ResourcesGained);
        }

        PlayerResourceManager.PlayerLoses(results.ResourcesLost);
        var resultsText = results.GetResultsString();
        Debug.Log(resultsText);
    }
}