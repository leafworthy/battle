using System;
using System.Resources;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GameController : Singleton<GameController>
{
    [SerializeField] private ResourceDefaultsSO resourceDefaultsSo;
    public Color PlayerColor;
    
    private void OnButtonPress(ResourceButton button)
    {
        switch (button.GetButtonType())
        {
            case ButtonType.TradeResource:
                PlayerResourceManager.Trade(button);
                break;
            case ButtonType.Sleep:
                DayController.Sleep();
                break;
            case ButtonType.Charm:
                UIManager.OpenMenu(MenuType.charm);
                break;
            case ButtonType.Attack:
                BattleManager.PlayerPressesAttackButton();
                break;
            case ButtonType.MainMenu:
               UIManager.OpenMenu(MenuType.main);
                break;
            case ButtonType.Retreat:
                BattleManager.Retreat();
                break;
            case ButtonType.SellAll:
                PlayerResourceManager.SellAll(button);
                break;
            case ButtonType.Fight:
                BattleManager.Fight();
                break;

            default:
                break;
        }
    }

    private void Start()
    {
        UIManager.Init();
        PlayerResourceManager.Init(resourceDefaultsSo);
        BattleManager.Init();
        ResourceButton.OnClickButton += OnButtonPress;
        DayController.Init();
        UIManager.OpenMenu(MenuType.main);
    }

}