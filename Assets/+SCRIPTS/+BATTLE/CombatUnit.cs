﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class CombatUnit : MonoBehaviour
{
    private Animator _anim;

    private CombatUnit _currentTarget;
    private Actions _currentAction;
    private Health _health;
    private Movement _move;
    private BattleTeam _owner;

    public float damageAmount = 10;
    public float specialDamageAmount = 40;
    public bool dead = false;


    public System.Action<CombatUnit> OnDie;
    public ResourceType unitType;
    private static readonly int Dead = Animator.StringToHash("Dead");
    private bool _retreating = false;
    private bool _cheering = false;

    void Awake()
    {
        _anim = GetComponent<Animator>();
        _move = GetComponent<Movement>();
        _health = GetComponent<Health>();
    }

    public void SetOwner(BattleTeam newOwner)
    {
        _owner = newOwner;
    }

    public BattleTeam GetOwner()
    {
        return _owner;
    }

    private void ResetAllBools()
    {
        string[] ActionTypeNames = System.Enum.GetNames(typeof(Actions));
        for (int i = 0; i < ActionTypeNames.Length; i++)
        {
            if (HasParameter(ActionTypeNames[i]))
            {
                _anim.SetBool(ActionTypeNames[i], false);
            }
        }
    }

    public bool HasParameter(string paramName)
    {
        if (_anim == null)
            return false;
        foreach (AnimatorControllerParameter param in _anim.parameters)
        {
            if (param.name == paramName)
                return true;
        }

        return false;
    }

    public void AttackHit()
    {
        if (TargetIsValid(_currentTarget))
        {
            _currentTarget.TakeDamage(damageAmount);
            _currentTarget.Push(damageAmount, transform);
        }
       
    }

    private void Push(float f, Transform pusher)
    {
        _move.Push(damageAmount, pusher);
    }

    public void TakeDamage(float damage)
    {
        if (_health == null)
        {
            return;
        }

        _health.TakeDamage(damage);
    }

    public void StatusEffect()
    {
        if (_health == null)
        {
            return;
        }
        _health.StatusEffect();
    }
    
    public void SpecialHit()
    {
        if (TargetIsValid(_currentTarget))
        {
            _currentTarget.TakeDamage(specialDamageAmount);
            _currentTarget.Push(specialDamageAmount, transform);
        }
    }

    private Actions GetRandomAction()
    {
        var all = System.Enum.GetValues(typeof(Actions));
        int randomIndex = Random.Range(0, all.Length);
        return (Actions) all.GetValue(randomIndex);
    }

    public void Think()
    {
        if (!dead)
        {
            ResetAllBools();
            Actions action = GetNextAction();

            SetAction(action);
        }
    }

    public void SetAction(Actions action)
    {
        _currentAction = action;
        if (HasParameter(action.ToString()))
        {
            _anim.SetBool(action.ToString(), true);
        }
    }

    private Actions GetNextAction()
    {
        if (_retreating)
        {
            return Actions.Retreating;
        }

        if (_cheering)
        {
            return Actions.Cheering;
        }

        _currentTarget = BattleManager.FindTarget(this);

        if (unitType != ResourceType.wraith)
        {
            if (TargetIsValid(_currentTarget))
            {
                return _move.CloseEnoughToTarget(_currentTarget) ? GetRandomAttackAction() : Actions.Running;
            }
        }
        else
        {
            return _move.CloseEnoughToTarget(_currentTarget) ? Actions.Standing : Actions.Running;
        }
        

        return Actions.Standing;
    }

    public bool TargetIsValid(CombatUnit target)
    {
        if (target.unitType == ResourceType.wraith)
            return false;
        if (unitType == ResourceType.wraith && target != null)
            return true;
        return (target != null && !target.dead && (target.unitType != ResourceType.wraith) &&
                target.GetOwner() != _owner);
    }

    private Actions GetRandomAttackAction()
    {
        if (unitType == ResourceType.wraith)
        {
            return Actions.Standing;
        }
        int randomIndex = Random.Range(0, 6);
        switch (randomIndex)
        {
            case 1:
                return Actions.Attack1;
            case 2:
                return Actions.Attack2;
            case 3:
                return Actions.Attack3;
            case 4:
                return Actions.Attack4;
            case 5:
                return Actions.SpecialAttack;
        }

        return Actions.Attack1;
    }


    public enum Actions
    {
        Attack1,
        Attack2,
        Attack3,
        Attack4,
        SpecialAttack,
        Running,
        Cheering,
        Retreating,
        Standing
    }


    // Update is called once per frame
    void Update()
    {
        if (!dead)
        {
            switch (_currentAction)
            {
                case Actions.Running:
                    if (TargetIsValid(_currentTarget) && !_move.CloseEnoughToTarget(_currentTarget))
                    {
                        _move.MoveTowardEnemy(_currentTarget);
                    }
                    else
                    {
                        Think();
                    }

                    break;
                case Actions.Cheering:
                    Think();
                    break;
                case Actions.Retreating:
                    _move.Retreat(_owner);
                    break;
            }
        }
    }


    public void Die()
    {
        ResetAllBools();
        _anim.SetBool(Dead, true);
        Destroy(GetComponent<Rigidbody2D>());
        Destroy(GetComponent<CircleCollider2D>());
        dead = true;
        OnDie?.Invoke(this);
    }

    public void RemoveGameObject()
    {
        //Destroy(gameObject);
    }

    public void Retreat()
    {
        if (!dead)
        {
            _retreating = true;
            _cheering = false;
            SetAction(Actions.Retreating);
        }
    }

    public void Cheer()
    {
        if (!dead)
        {
            _cheering = true;
            SetAction(Actions.Cheering);
        }
    }

    public void DoSpecial()
    {
        SetAction(Actions.SpecialAttack);
    }

    public void DoLightningStart()
    {
        var units = _owner.GetOpponent().GetUnits();
        foreach (CombatUnit combatUnit in units)
        {
            if (!combatUnit.dead)
            {
                combatUnit.StatusEffect();
            }
        }
    }

    public void DoLightningHit()
    {
        var units = _owner.GetOpponent().GetUnits();
        foreach (CombatUnit combatUnit in units)
        {
            if (!combatUnit.dead)
            {
                combatUnit.TakeDamage(50);
            }
        }
    }
}