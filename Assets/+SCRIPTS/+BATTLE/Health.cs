using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[ExecuteInEditMode]
public class Health: MonoBehaviour
{
        public float healthAmount = 100;
        public float healthMax = 100;
        private CombatUnit combat;
        public bool dead = false;
        public Bar healthBar;
        [FormerlySerializedAs("HitByLightning")] public Action OnHitByLightning;

        private void Start()
        {
                combat = GetComponent<CombatUnit>();
                healthBar.UpdateBar(0,healthMax);
        }

        private void OnValidate()
        {
                healthBar = GetComponentInChildren<Bar>();
        }

        public void TakeDamage(float amount)
        {
                if (!dead)
                {
                        healthAmount -= amount;
                        if (healthAmount <= 0)
                        {
                                if (!dead)
                                {
                                        dead = true;
                                        healthBar.gameObject.SetActive(false); 
                                        combat.Die();
                                }
                        }

                        healthBar.UpdateBar(healthMax-healthAmount, healthMax);
                }
                else
                {
                        healthBar.gameObject.SetActive(false); 
                }
        }

        public void StatusEffect()
        {
                OnHitByLightning?.Invoke();
        }
}