﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class DialogueBox : MonoBehaviour
{
	[FormerlySerializedAs("YesButton")] public Button yesButton;
	[FormerlySerializedAs("NoButton")] public Button noButton;
	[FormerlySerializedAs("DescriptionText")] public Text descriptionText;
	UnityAction onClickYes;
	UnityAction onClickNo;
	public HorizontalLayoutGroup horizontalLayoutGroup;


	private void NoButtonClick()
	{
		Hide();
		onClickNo();
	}


	private void YesButtonClick()
	{
		Hide();
		onClickYes();
	}

	internal void Show(string description, UnityAction onClickYes, UnityAction onClickNo)
	{

		gameObject.SetActive(true);
		noButton.gameObject.SetActive(true);

		descriptionText.text = description;

		this.onClickYes = onClickYes;
		this.onClickNo = onClickNo;

		yesButton.onClick.AddListener(YesButtonClick);
		noButton.onClick.AddListener(NoButtonClick);

	}

	internal void Hide()
	{
		yesButton.onClick.RemoveAllListeners();
		noButton.onClick.RemoveAllListeners();
		descriptionText.text = "";
		gameObject.SetActive(false);
	}

	internal void Show(string description, UnityAction onClickYes)
	{
		gameObject.SetActive(true);
		noButton.gameObject.SetActive(false);
		descriptionText.text = description;

		this.onClickYes = onClickYes;

		yesButton.onClick.AddListener(YesButtonClick);
	}
}
