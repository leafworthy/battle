using TMPro;

[System.Serializable]
public class MainMenu: Menu
{
    public TextMeshProUGUI BattleResultsText;
    public override void On()
    {
        base.On();
        UIManager.ClearText();
        
        UIManager.DisplayMainMenuText();
    }

    
}