using UnityEngine.Events;
using UnityEngine.Serialization;

[System.Serializable]
public class ButtonAction
{
	[FormerlySerializedAs("Name")] public string name;
	public UnityAction action;
	public ButtonAction(string name, UnityAction action)
	{
		this.name = name;
		this.action = action;
	}
}
