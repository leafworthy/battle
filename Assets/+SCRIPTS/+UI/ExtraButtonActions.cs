﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ExtraButtonActions : MonoBehaviour
{
    public UnityAction OnAction;
    // Start is called before the first frame update
    void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnAction);
    }

    // Update is called once per frame
    void Update()
    {
        if (OnAction != null)
        {
            OnAction.Invoke();
        }
    }
}
