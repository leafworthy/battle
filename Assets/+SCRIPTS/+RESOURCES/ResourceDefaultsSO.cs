using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Resources Defaults", menuName = "ScriptableObjects/ResourceDefaultsSO", order = 1)]
public class ResourceDefaultsSO : ScriptableObject
{
    [SerializeField] public ResourcesSO DefaultPlayerResources;
    [SerializeField] public ResourcesSO MaxAttackerResourcesScriptableObject;
    [SerializeField] public ResourcesSO MaxDefenderResourcesScriptableObject;

}