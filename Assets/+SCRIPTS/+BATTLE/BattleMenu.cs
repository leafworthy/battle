using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;

[System.Serializable]
public class BattleMenu : Menu
{
    private static BattleTeam _currentAttacker;
    private static BattleTeam _currentDefender;
    public TextMeshProUGUI attackerResources;
    public TextMeshProUGUI defenderResources;

    public static void SetTeams(BattleTeam attacker, BattleTeam defender)
    {
        _currentAttacker = attacker;
        _currentDefender = defender;

    }

    public override void On()
    {
        base.On();
        attackerResources.text = _currentAttacker.Loot.GetLootResourcesText();
        defenderResources.text = _currentDefender.Loot.GetLootResourcesText();
    }

    void Update()
    {
        attackerResources.text = _currentAttacker.Loot.GetLootResourcesText();
        defenderResources.text = _currentDefender.Loot.GetLootResourcesText();
    }

    public override void Off()
    {
        BattleManager.CleanUp();
        base.Off();
    }
}