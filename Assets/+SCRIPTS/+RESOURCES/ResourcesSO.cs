﻿using System.Collections.Generic;
 using UnityEngine;
 [System.Serializable]
 [CreateAssetMenu(fileName = "Resources Data", menuName = "ScriptableObjects/ResourcesSO", order = 1)]
 public class ResourcesSO : ScriptableObject
 {
     public string Name;
     [SerializeField]private List<Resource> resourceList = new List<Resource>();

     public ResourceList Resources => GetResourceList();

     public ResourceList GetResourceList()
     {
         var newList = new ResourceList();
         newList.Gain(resourceList);
         return newList;
     }
 }