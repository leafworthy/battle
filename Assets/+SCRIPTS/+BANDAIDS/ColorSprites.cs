﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class ColorSprites : MonoBehaviour
{
	[SerializeField]private Color currentColor;
	[SerializeField]private List<SpriteRenderer> sprites = new List<SpriteRenderer> ();
	// Use this for initialization
	private void Update()
	{
		sprites.Clear();
		foreach (SpriteRenderer sr in GetComponentsInChildren<SpriteRenderer>())
		{
			sprites.Add(sr);
			//sr.sortingOrder = 0;
		}

		Recolor(currentColor);
	}


	public void Recolor (Color c)
	{
		currentColor = c;
		foreach (SpriteRenderer sr in GetComponentsInChildren <SpriteRenderer>()) {
			if(sr.tag != "dontcolor")
			sr.color = currentColor;
		}

	}




}
