﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public int _dir = -1;
    public GameObject scaleObject;
    public float closeEnoughDistance = .5f;
    public float walkSpeed = 2;
    private Vector2 velocity = Vector2.zero;
    private float minimumVelocity = .1f;
    private float inirtiaFactor = .8f;
    private float pushFactor = .15f;

    private void Update()
    {
        if (velocity != Vector2.zero && velocity.magnitude <= minimumVelocity)
        {
            velocity = Vector2.zero;
        }
        else
        {
            velocity *= inirtiaFactor;
            transform.position += (Vector3) velocity * Time.deltaTime;
        }
    }

    public bool CloseEnoughToTarget(CombatUnit enemyTarget)
    {
        if (enemyTarget == null)
            return true;
        float distance = Vector2.Distance(gameObject.transform.position, enemyTarget.transform.position);

        if (distance <= closeEnoughDistance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void MoveTowardEnemy(CombatUnit enemyTarget)
    {
        var enemyDir = (enemyTarget.transform.position - transform.position).normalized;
        transform.position += enemyDir * walkSpeed * Time.deltaTime;
        FaceDir(enemyTarget.transform);
    }

    private void MoveTowardSide(bool toRight)
    {
        var dirVector = toRight ? Vector2.right : Vector2.left;
        transform.position += (Vector3) dirVector * walkSpeed * Time.deltaTime;
        FaceDir(toRight);
    }

    private void FaceDir(Transform enemyTarget)
    {
        if (enemyTarget.transform.position.x > transform.position.x)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y,
                transform.localScale.z);
        }
        else
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1, transform.localScale.y,
                transform.localScale.z);
        }
    }

    private void FaceDir(bool toRight)
    {
        if (toRight)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y,
                transform.localScale.z);
        }
        else
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -1, transform.localScale.y,
                transform.localScale.z);
        }
    }

    public void Push(float damageAmount, Transform pusher)
    {
        var dir = (transform.position - pusher.position).normalized;
        velocity += (Vector2) dir * damageAmount * pushFactor;
    }

    public void Retreat(BattleTeam owner)
    {
        MoveTowardSide(!owner.isAttacker);
    }
}