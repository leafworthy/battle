﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffect : MonoBehaviour
{
    [SerializeField]private Health _health;

    private Animator _anim;
    private void Start()
    {
        _health = GetComponentInParent<Health>();
        _health.OnHitByLightning += OnHit;
        _anim = GetComponent<Animator>();
    }

    private void OnHit()
    {
       _anim.SetTrigger("Hit");
       Debug.Log("hit");
    }

 
}
