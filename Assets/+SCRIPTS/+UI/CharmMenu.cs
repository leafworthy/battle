using System;
using UnityEngine;

[System.Serializable]
public class CharmMenu : Menu
{
    public ButtonControl CharmButton;
    private int charmedToday = 0;
    private int attemmptsToday = 0;

    public override void On()
    {
        base.On();
        CharmButton.onScore01 += Charm;
        charmedToday = 0;
        attemmptsToday = 0;
        DisplayWilling();
    }

    private void DisplayWilling()
    {
        UIManager.ClearText();

        UIManager.DisplayText("There are " + PlayerResourceManager.GetAmount(ResourceType.spee) +
                              " people willing to listen today.");
    }

    private void Update()
    {
        CharmButton.disabled = PlayerResourceManager.GetAmount(ResourceType.spee) < 1;
    }

    public override void Off()
    {
        base.Off();
        CharmButton.onScore01 -= Charm;
    }

    public void Charm(bool score)
    {
        PlayerResourceManager.Charm(score);
        if (score)
        {
            charmedToday++;
        }

        attemmptsToday++;
        DisplayWilling();
        DisplayCharmed();
    }

    private void DisplayCharmed()
    {
        UIManager.DisplayText("\nFollowers Charmed Today: " + charmedToday + " / " + attemmptsToday);
    }
}