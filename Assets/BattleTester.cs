﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTester : MonoBehaviour
{
    public BattleTeam attacker;
    public BattleTeam defender;

    void Start()
    {
        attacker.Loot.Gain(attacker.startingLoot);
        defender.Loot.Gain(defender.startingLoot);

    }

[ContextMenu("StartTestBattle")]
    public void StartBattle()
    {
       //BattleManager.StartNewBattle(attacker, defender);
    }

    public void HumanCastSpell()
    {
        BattleManager.HumanCastSpell();
    }
    
}
